# OpenCV PCB Recognition Project
- Constructed by Aubrey Jones

## Sections and Explanations

- [Code](https://gitlab.com/Aubrey.Jones/opencv-pcb-recognition/-/tree/main/Code) - This is where any codes relevant to the finalized project or the development will be found. Currently there is only a Frame Clipper here but more is being made.
- [Diagrams](https://gitlab.com/Aubrey.Jones/opencv-pcb-recognition/-/tree/main/Diagrams) - The Diagrams that were created during the duration of the project. 
- [Dataset](https://gitlab.com/Aubrey.Jones/opencv-pcb-recognition/-/tree/main/Data%20Sets) - The current PCB Object Recognition Dataset. This isn't very useful without the accompanying code, but that is being worked on and will be finished soon.
